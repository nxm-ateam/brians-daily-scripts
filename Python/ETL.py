import csv

def fetch_table_schemas() -> dict:
    table_schemas = dict()
    table_schemas['om_load'] = ['server_ts_date', 'nts', 'api_key', 'uid', 'step']
    table_schemas['om_revenue'] = ['server_ts_date', 'nts', 'api_key', 'uid', 'purchased', 'name', 'currency_code',
                                   'total']
    return table_schemas


def parse(in_file,table_schemas):
    parsed_data = {}
    for x in table_schemas.keys():
        parsed_data[x] = list()
    for line in in_file:
        #break down the csv
        temp = line.split(',')
        #get nts and server_ts_date from the first and third columns
        values = {'nts': temp[0], 'server_ts_date': temp[2]}
        #now get the rest of the data
        values.update([(x.split('=')[0], x.split('=')[1]) for x in temp[7].split('&')])
        #now add those values to the appropriate group of events
        if 'om_event_type' in values:
            parsed_data[values['om_event_type']].append(values)
    return parsed_data


def etl(filename):
    table_schemas = fetch_table_schemas()
    parsed_data = parse(open(str(filename), 'r'), table_schemas)
    for x in table_schemas.keys():
        if x in parsed_data.keys():
            with open(str(x), 'w') as out_file:
                writer = csv.DictWriter(out_file, fieldnames=table_schemas[x], extrasaction='ignore')
                writer.writerows(parsed_data[x])
