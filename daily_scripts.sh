#!/bin/bash -x

TARGET_DIR=/home/rstudio/Brian/daily_scripts

if [ $# -eq 0 ]; then
  DATE=`date --date='yesterday' "+%Y-%m-%d"`
else
	DATE=$1
fi

for f in $TARGET_DIR/monstersquad/*; do echo "executing $f"; Rscript $f; done;

for f in $TARGET_DIR/basebusters/*; do echo "executing $f"; Rscript $f; done;

for f in $TARGET_DIR/legion/*; do echo "executing $f"; Rscript $f; done;

Rscript $TARGET_DIR/warwings/war_wings_aggregations.R $DATE

for f in $TARGET_DIR/legacyquest/*; do echo "executing $f"; Rscript $f; done;

for f in $TARGET_DIR/rebellion/*; do echo "executing $f"; Rscript $f; done;