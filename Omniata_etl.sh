#!/bin/bash -x

TARGET_DIR=/home/rstudio/Brian/omniata_data

if [ $# -eq 0 ]; then
	DATE=`date --date='yesterday' "+%Y-%m-%d"`
  FILE=`date --date='yesterday' "+%Y-%m-%d"`
else
  DATE=$1
  FILE=$2
fi

sshpass -p '5T3v3' sftp nexon@sftp.omniata.com:$FILE.csv.gz $TARGET_DIR/raw/
gunzip $TARGET_DIR/raw/$FILE.csv.gz

cat $TARGET_DIR/raw/$FILE.csv | grep 'api_key=9c6e3b04\|api_key=700b6c90\|api_key=0840778f\|api_key=aafae37d\|api_key=b022f740' | grep 'om_event_type=om_load\|om_event_type=om_revenue' > $TARGET_DIR/raw/$FILE.new.csv

rm $TARGET_DIR/raw/$FILE.csv

Rscript /home/rstudio/Brian/daily_scripts/Omniata_etl.R $FILE

s3cmd put $TARGET_DIR/processed/$FILE.csv s3://anlayst-brian-bucket

Rscript /home/rstudio/Brian/daily_scripts/Omniata_etl_load.R $DATE $FILE